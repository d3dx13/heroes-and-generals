# Heroes and Generals

Heroes & Generals changelog

This repository will be updated with a list of files each time an update is released for the purpose of finding new features in the game.

#### If the current version of the game is different than the one listed in the commit, please notify me.

**My contacts:**

vk: https://vk.com/d3dx13

telegram: https://t.me/d3dx13

mail: zhidkov2846@gmail.com
